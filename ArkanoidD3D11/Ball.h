#pragma once
#include "Object.h"
class Ball :
    public Object
{
private:
    float speed;
    XMFLOAT2* currentVelocity;

public:
    Ball(unsigned int transformIndex, unsigned int UVTransformIndex, float halfWidth, float halfHeight, float Speed);
    ~Ball();
    float Speed() const;
    XMFLOAT2* CurrentVelocity() const;
};

