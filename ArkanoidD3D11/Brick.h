#pragma once
#include "Object.h"
class Brick :
    public Object
{
private:
    int hitsRemaining;

public:
    Brick(unsigned int transformIndex, unsigned int UVTransformIndex, float halfWidth, float halfHeight);
    
    int HitsRemaining() const;
    void SetHitsRemaining(int hits_remaining);
    bool DecreaseHitsRemaining();
};

