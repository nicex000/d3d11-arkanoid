#include "Paddle.h"

Paddle::Paddle(unsigned transformIndex, unsigned UVTransformIndex, float halfWidth, float halfHeight, float Speed):
	Object(transformIndex, UVTransformIndex, halfWidth, halfHeight), speed(Speed)
{
}

float Paddle::Speed() const
{
	return speed;
}
