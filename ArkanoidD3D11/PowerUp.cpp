#include "PowerUp.h"

PowerUp::PowerUp(unsigned transformIndex, unsigned UVTransformIndex, float halfWidth, float halfHeight, float Speed):
	Object(transformIndex, UVTransformIndex, halfWidth, halfHeight), fallSpeed(Speed)
{
}

float PowerUp::FallSpeed() const
{
	return fallSpeed;
}
