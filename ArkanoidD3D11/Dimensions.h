#pragma once

namespace ArkanoidGame
{
	constexpr unsigned gk_entitiesCount = 6; //bricks + ball + player + arena + bonus + logo
	constexpr unsigned gk_bricksCount = 120;
	constexpr unsigned gk_arenaHalfWidth = 20;
	constexpr unsigned gk_arenaHalfHeight = 30;
	constexpr unsigned gk_bricksHalfWidth = 2;
	constexpr unsigned gk_bricksHalfHeight = 1;
	constexpr unsigned gk_bricksWidth = gk_bricksHalfWidth * 2;
	constexpr unsigned gk_bricksHeight = gk_bricksHalfHeight * 2;
	constexpr unsigned gk_maxBonusBricksHitCount = 5;
	static constexpr float gk_gameOverBallYOffset = 15.0f;
	static constexpr float gk_destroyPowerUpYOffset = 1.0f;


	//1 ball + 1 player + 1 arena + 1 bonus + gk_bricksCount bricks
	constexpr unsigned int gk_instancesCount = (gk_entitiesCount - 1) + gk_bricksCount; 
}