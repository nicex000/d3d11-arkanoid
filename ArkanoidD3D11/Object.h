#pragma once
#include <DirectXMath.h>

using namespace DirectX;

class Object
{
protected:
	XMFLOAT4* transformMatrix;
	XMFLOAT4* color;
	XMFLOAT4* uvTransform;

	XMFLOAT2 halfExtents;
	unsigned int index;
	unsigned int UVIndex;
public:
	Object(unsigned int transformIndex, unsigned int UVTransformIndex, float halfWidth, float halfHeight);

	void Init(XMFLOAT4* initialTransform, XMFLOAT4* initialColor, XMFLOAT4* uv);

	XMFLOAT2 HalfExtents() const;
	float HalfWidth() const;
	float HalfHeight() const;

	unsigned Index() const;
	unsigned UvIndex() const;

	XMFLOAT4& Transform();
	const XMFLOAT4& Transform() const;
	void SetTransform(XMFLOAT4&& transform_matrix);

	XMFLOAT4& Color();
	const XMFLOAT4& Color() const;
	void SetColor(XMFLOAT4&& color);
	void SetColor(const XMFLOAT4& color);

	XMFLOAT4& UvTransform();
	const XMFLOAT4& UvTransform() const;
	void SetUvTransform(XMFLOAT4&& uv_transform);
};

