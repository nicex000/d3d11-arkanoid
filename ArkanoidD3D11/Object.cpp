#include "Object.h"

#include <algorithm>

Object::Object(unsigned int transformIndex, unsigned int UVTransformIndex, float halfWidth, float halfHeight):
																			transformMatrix(nullptr),
																			color(nullptr),
                                                                            uvTransform(nullptr),
																			halfExtents(halfWidth, halfHeight),
                                                                            index(transformIndex),
                                                                            UVIndex(UVTransformIndex)
{
}

void Object::Init(XMFLOAT4* initialTransform, XMFLOAT4* initialColor, XMFLOAT4* uv)
{
	transformMatrix = initialTransform;
	color = initialColor;
	uvTransform = uv;
}

XMFLOAT2 Object::HalfExtents() const
{
	return halfExtents;
}

float Object::HalfWidth() const
{
	return halfExtents.x;
}

float Object::HalfHeight() const
{
	return halfExtents.y;
}

unsigned Object::Index() const
{
	return index;
}

unsigned Object::UvIndex() const
{
	return UVIndex;
}

XMFLOAT4& Object::Transform()
{
	return *transformMatrix;
}

const XMFLOAT4& Object::Transform() const
{
	return *transformMatrix;
}

void Object::SetTransform(XMFLOAT4&& transform_matrix)
{
	*transformMatrix = std::move(transform_matrix);
}

XMFLOAT4& Object::Color()
{
	return *color;
}

const XMFLOAT4& Object::Color() const
{
	return *color;
}

void Object::SetColor(XMFLOAT4&& color)
{
	*this->color = std::move(color);
}

void Object::SetColor(const XMFLOAT4& color)
{
	*this->color = color;
}

XMFLOAT4& Object::UvTransform()
{
	return *uvTransform;
}

const XMFLOAT4& Object::UvTransform() const
{
	return *uvTransform;
}

void Object::SetUvTransform(XMFLOAT4&& uv_transform)
{
	*uvTransform = uv_transform;
}
