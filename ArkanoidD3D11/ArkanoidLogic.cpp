#include "MemoryCommon.h"
#include "ArkanoidLogic.h"
#include "Application.h"
#include "Window.h"
#include "TextureTileInfo.h"
#include <fstream>
#include "Resources.h"
#include <unordered_map>
#include "AABB.h"
#include <ctime>

#include "Ball.h"
#include "Object.h"

using namespace ArkanoidGame;

//static constexpr float m_arena->OutOfArenaX() = m_arena->Bounds().MaxX * 10.0f;


static constexpr unsigned int gk_brickTypesCount = 3;

static const XMFLOAT4 gk_bricksColors[gk_brickTypesCount] =
{
	XMFLOAT4{ 1.0f, 0.0f, 0.0f, 1.0f },
	XMFLOAT4{ 0.0f, 1.0f, 0.0f, 1.0f },
	XMFLOAT4{ 0.0f, 0.0f, 1.0f, 1.0f }
};

static unsigned int getColorIndex(const XMFLOAT4& color)
{
	if (color.x == 1.f) return 0;
	if (color.y == 1.f) return 1;
	return 2;
}

static constexpr unsigned int gk_bricksHitsCounts[gk_brickTypesCount] =
{
	3,
	2,
	1
};



static ArkanoidLogic::Quadtree createQuadtree(const AABB& bricksAABB, const XMFLOAT2& bricksHalfExtents)
{
	return ArkanoidLogic::Quadtree{ bricksAABB, bricksHalfExtents };
}

static ArkanoidLogic::Quadtree createQuadtree(const XMFLOAT2& bricksHalfExtents)
{
	const AABB arenaAABB = AABB::computeFromCenterAndHalfExtents(XMFLOAT2{ 0.0f, 0.0f },
		XMFLOAT2{ static_cast<float>(gk_arenaHalfWidth),
				  static_cast<float>(gk_arenaHalfHeight) });
	return createQuadtree(arenaAABB, bricksHalfExtents);
}

ArkanoidLogic::ArkanoidLogic(Application& application) : m_application{ application },
m_inputManager{ application.window(), *this },
m_quadtree{ createQuadtree(XMFLOAT2{ gk_bricksHalfWidth, gk_bricksHalfHeight }) }
{
	updateCameraProjection();
	m_camera.lookAt(XMFLOAT3{ 0.0f, 0.0f, -35.0f }, XMFLOAT3{ 0.0f, 0.0f, 0.0f });

	std::srand(static_cast<unsigned int>(std::time(nullptr)));

	InitObjects();
	SetupLevel();

	m_renderer = new ArkanoidRenderer{ *this };

	application.window().addWindowSizeEventsObserver(this);
}

ArkanoidLogic::~ArkanoidLogic()
{
	delete m_arena;
	for (int i = 0; i < gk_bricksCount; ++i)
	{
		delete m_bricks[i];
	}
	delete m_paddle;
	delete m_powerUp;
	delete m_logo;


	m_application.window().removeWindowSizeEventsObserver(this);
	delete m_renderer;
}

void ArkanoidLogic::InitObjects()
{
	unsigned tranformIndex = 0;
	unsigned UVTranformIndex = 0;

	//Arena
	m_arena = new Arena(tranformIndex++, UVTranformIndex++,
		gk_arenaHalfWidth, gk_arenaHalfHeight);
	m_arena->Init(TransformItem(m_arena->Index()), ColorItem(m_arena->Index()),
		UvTransformItem(m_arena->UvIndex()));

	//Bricks
	for (int i = 0; i < gk_bricksCount; ++i)
	{
		m_bricks[i] = new Brick(tranformIndex, UVTranformIndex,
			gk_bricksHalfWidth, gk_bricksHalfHeight);
		m_bricks[i]->Init(TransformItem(m_bricks[i]->Index()),
			ColorItem(m_bricks[i]->Index()), UvTransformItem(m_bricks[i]->UvIndex()));
		++tranformIndex;
	}
	++UVTranformIndex;


	//Ball
	m_ball = new Ball(tranformIndex++, UVTranformIndex++,
		1.f, 1.f, 35.f);
	m_ball->Init(TransformItem(m_ball->Index()), ColorItem(m_ball->Index()),
		UvTransformItem(m_ball->UvIndex()));

	//Paddle
	m_paddle = new Paddle(tranformIndex++, UVTranformIndex++,
		3.f, 0.5f, 24.f);
	m_paddle->Init(TransformItem(m_paddle->Index()), ColorItem(m_paddle->Index()),
		UvTransformItem(m_paddle->UvIndex()));

	//Power Up
	m_powerUp = new PowerUp(tranformIndex++, UVTranformIndex++,
		1.f, 0.5f, 8.f);
	m_powerUp->Init(TransformItem(m_powerUp->Index()), ColorItem(m_powerUp->Index()),
		UvTransformItem(m_powerUp->UvIndex()));

	//Logo
	m_logo = new Object(tranformIndex++, UVTranformIndex++, 20.f, 5.f);
	m_logo->Init(TransformItem(m_logo->Index()), ColorItem(m_logo->Index()),
		UvTransformItem(m_logo->UvIndex()));
}

void ArkanoidLogic::SetupLevel()
{
	//Arena
	m_arena->SetTransform(XMFLOAT4{ 0.0f, 0.0f,
		m_arena->Bounds().MaxX + 1.5f, m_arena->Bounds().MaxY + 1.5f });
	m_arena->SetColor(XMFLOAT4{ 1.0f, 1.0f, 1.0f, static_cast<float>(m_arena->UvIndex()) });

	//Bricks
	placeBricks();

	//Ball
	m_ball->SetTransform(XMFLOAT4{ 0.0f, m_arena->Bounds().MinY + 2.0f,
		m_ball->HalfWidth(), m_ball->HalfHeight() });
	m_ball->SetColor(XMFLOAT4{ 1.0f, 1.0f, 1.0f, static_cast<float>(m_ball->UvIndex()) });

	XMVECTOR ballVelocity = XMVectorScale(XMVector2Normalize(XMVectorSet(0.5f, 1.f, 0.0f, 0.0f)), m_ball->Speed());
	XMStoreFloat2(m_ball->CurrentVelocity(), ballVelocity);

	//Player
	m_paddle->SetTransform(XMFLOAT4{ 0.0f, m_arena->Bounds().MinY,
		m_paddle->HalfWidth(), m_paddle->HalfHeight() });
	m_paddle->SetColor(XMFLOAT4{ 1.0f, 1.0f, 1.0f, static_cast<float>(m_paddle->UvIndex()) });


	//Power Up
	m_powerUp->SetTransform(XMFLOAT4{ m_arena->OutOfArenaX(), 0.f,
		m_powerUp->HalfWidth(), m_powerUp->HalfHeight() });
	m_powerUp->SetColor(XMFLOAT4{ 1.0f, 1.0f, 1.0f, static_cast<float>(m_powerUp->UvIndex()) });

	//Logo
	m_logo->SetTransform(XMFLOAT4{ m_arena->Width() + 5.0f, m_arena->HalfHeight() * 0.5f,
		m_logo->HalfWidth(), m_logo->HalfHeight() });
	m_logo->SetColor(XMFLOAT4{ 1.0f, 1.0f, 1.0f, static_cast<float>(m_logo->UvIndex()) });

	m_bonusAlive = false;
	m_bonusBricksHit = 0;
	m_nextBonusBricksHitCount = generateNextBonusBricksHitCount();
}

void ArkanoidLogic::updateCameraProjection()
{
	m_camera.makePerspective(m_application.window().aspectRatio(), XM_PI * 0.5f, 1.0f, 50.0f);
}

void ArkanoidLogic::onResize()
{
	updateCameraProjection();
	m_renderer->onCameraChanged();
}

unsigned int ArkanoidLogic::generateNextBonusBricksHitCount()
{
	return (std::rand() % gk_maxBonusBricksHitCount) + 1;
}

void ArkanoidLogic::placeBricks()
{
	//place bricks picking randomly one of the brick placer functions
	AABB bricksAABB{};
	const unsigned int brickPlacerFunctionIndex = std::rand() % sk_brickPlacersCount;
	const unsigned int placedBricks = (this->*sk_brickPlacers[brickPlacerFunctionIndex])(bricksAABB);

	buildQuadtree(bricksAABB, placedBricks);

	//hide unplaced bricks
	for (unsigned int unPlacedBrickIndex = placedBricks; unPlacedBrickIndex < gk_bricksCount; ++unPlacedBrickIndex)
	{
		translateOutOfArena(m_bricks[unPlacedBrickIndex]->Transform());
	}
}

void ArkanoidLogic::buildQuadtree(const AABB& bricksAABB, unsigned int bricksCount)
{
	m_quadtree = createQuadtree(bricksAABB, m_bricks[0]->HalfExtents());

	for (unsigned int brickIndex = 0; brickIndex < bricksCount; ++brickIndex)
	{
		
		const XMFLOAT4& brickTranslationAndScale = m_bricks[brickIndex]->Transform();
		const XMFLOAT2 brickCenter{ brickTranslationAndScale.x, brickTranslationAndScale.y };
		m_quadtree.insert(brickCenter, brickIndex);
	}
}

void ArkanoidLogic::restartLevel()
{
	SetupLevel();
	m_renderer->onColorsOrUVTransformsChanged();
}

void ArkanoidLogic::render()
{
	m_renderer->render();
}

XMFLOAT4* ArkanoidLogic::TransformItem(unsigned index)
{
	return &m_transforms.translationAndScales[index];
}

XMFLOAT4* ArkanoidLogic::ColorItem(unsigned index)
{
	return &m_colorsAndUVTransforms.colorScaleAndIndex[index];
}

XMFLOAT4* ArkanoidLogic::UvTransformItem(unsigned index)
{
	return &m_colorsAndUVTransforms.uvTranslationAndScales[index];
}

void ArkanoidLogic::update()
{
	const float deltaTimeMillis = m_application.timer().deltaTime();
	const float deltaTime = static_cast<float>(deltaTimeMillis) / 1000.0f;

	movePlayer(deltaTime);
	moveBonus(deltaTime);

	const XMFLOAT4& ballTranslateAndScale = m_ball->Transform();

	const XMFLOAT2 lastBallPosition{ ballTranslateAndScale.x, ballTranslateAndScale.y };

	moveBall(deltaTime); //modifies ballTranslateAndScale

	const XMFLOAT2 ballPosition{ ballTranslateAndScale.x, ballTranslateAndScale.y };

	if (ballPosition.y < m_arena->Bounds().MinY - gk_gameOverBallYOffset)
	{
		restartLevel();
		return;
	}

	const XMFLOAT4& playerTranslateAndScale = m_paddle->Transform();
	const XMFLOAT2 playerPosition{ playerTranslateAndScale.x, playerTranslateAndScale.y };

	const AABB playerAABB = AABB::computeFromCenterAndHalfExtents(playerPosition, m_paddle->HalfExtents());
	const AABB currBallAABB = AABB::computeFromCenterAndHalfExtents(ballPosition, m_ball->HalfExtents());

	const XMFLOAT2 playerAABBMin = playerAABB.min();
	const XMFLOAT2 playerAABBMax = playerAABB.max();
	const XMFLOAT2 currBallAABBMin = currBallAABB.min();
	const XMFLOAT2 currBallAABBMax = currBallAABB.max();

	checkBounds(playerAABBMin, playerAABBMax, currBallAABBMin, currBallAABBMax);
	checkBonusCollision(playerAABB);

	const AABB lastBallAABB = AABB::computeFromCenterAndHalfExtents(lastBallPosition, m_ball->HalfExtents());
	const XMFLOAT2 lastBallAABBMin = lastBallAABB.min();
	const XMFLOAT2 lastBallAABBMax = lastBallAABB.max();

	if (playerAABB.intersects(currBallAABB))
	{
		CollisionData collisionData = ballAABBCollisionData(playerAABB, currBallAABBMin, currBallAABBMax, lastBallAABBMin, lastBallAABBMax);

		const unsigned int reverseYVelocity = collisionData.fromTop | collisionData.fromBottom;

		const float ballVelocitiesY[2] = { m_ball->CurrentVelocity()->y, -m_ball->CurrentVelocity()->y };

		m_ball->CurrentVelocity()->y = ballVelocitiesY[reverseYVelocity];

		//find ball AABB's x position nearest to the player
		const unsigned int isBallLeftSide = static_cast<unsigned int>(ballPosition.x < playerPosition.x);
		const float ballXPos[2] = { currBallAABBMin.x, currBallAABBMax.x };

		//make the new velocity angle (wrt the player normal) proportional to the hit distance
		const float ballXPosLocal = ballXPos[isBallLeftSide] - playerPosition.x;
		const float velocityXDir = (ballXPosLocal / m_paddle->HalfExtents().x); //normalize

		m_ball->CurrentVelocity()->x = m_ball->Speed() * velocityXDir;
	}
	else
	{
		checkBricksCollision(currBallAABB, currBallAABBMin, currBallAABBMax, lastBallAABBMin, lastBallAABBMax);
	}
}

void ArkanoidLogic::movePlayer(float deltaTime)
{
	XMFLOAT4& playerTranslationAndScale = m_paddle->Transform();

	const float displacementValue = m_paddle->Speed() * deltaTime;

	const float leftKeyDisplacements[2] = { 0.0f, -displacementValue };
	const float rightKeyDisplacements[2] = { 0.0f, displacementValue };

	playerTranslationAndScale.x += leftKeyDisplacements[static_cast<unsigned int>(m_inputManager.isLeftKeyPressed())];
	playerTranslationAndScale.x += rightKeyDisplacements[static_cast<unsigned int>(m_inputManager.isRightKeyPressed())];
}

void ArkanoidLogic::moveBall(float deltaTime)
{
	XMFLOAT4& ballTranslationAndScale = m_ball->Transform();
	ballTranslationAndScale.x += m_ball->CurrentVelocity()->x * deltaTime;
	ballTranslationAndScale.y += m_ball->CurrentVelocity()->y * deltaTime;
}

void ArkanoidLogic::moveBonus(float deltaTime)
{
	XMFLOAT4& bonusTranslateAndScale = m_powerUp->Transform();
	bonusTranslateAndScale.y -= static_cast<unsigned int>(m_bonusAlive) * m_powerUp->FallSpeed() * deltaTime;
}

void ArkanoidLogic::checkBounds(const XMFLOAT2& playerAABBMin, const XMFLOAT2& playerAABBMax,
	const XMFLOAT2& ballAABBMin, const XMFLOAT2& ballAABBMax)
{
	//player

	//adjust player position inside the arena

	XMFLOAT4& playerPos = m_paddle->Transform();

	const float playerPosXLeftBounds[2] = { playerPos.x, m_arena->Bounds().MinX + m_paddle->HalfExtents().x };

	playerPos.x = playerPosXLeftBounds[static_cast<unsigned int>(playerAABBMin.x < m_arena->Bounds().MinX)];

	const float playerPosXRightBounds[2] = { playerPos.x, m_arena->Bounds().MaxX - m_paddle->HalfExtents().x };

	playerPos.x = playerPosXRightBounds[static_cast<unsigned int>(playerAABBMax.x > m_arena->Bounds().MaxX)];

	//ball

	const unsigned int outOfArenaXright = static_cast<unsigned int>(ballAABBMax.x > m_arena->Bounds().MaxX);
	const unsigned int outOfArenaXleft = static_cast<unsigned int>(ballAABBMin.x < m_arena->Bounds().MinX);

	const unsigned int outOfArenaX = outOfArenaXright | outOfArenaXleft;

	const unsigned int outOfArenaYtop = static_cast<unsigned int>(ballAABBMax.y > m_arena->Bounds().MaxY);

	const float ballVelocitiesX[2] = { m_ball->CurrentVelocity()->x, -m_ball->CurrentVelocity()->x };
	const float ballVelocitiesY[2] = { m_ball->CurrentVelocity()->y, -m_ball->CurrentVelocity()->y };

	m_ball->CurrentVelocity()->x = ballVelocitiesX[outOfArenaX];
	m_ball->CurrentVelocity()->y = ballVelocitiesY[outOfArenaYtop];

	//adjust ball position inside the arena

	XMFLOAT4& ballPos = m_ball->Transform();

	const float ballPosXLeftBounds[2] = { ballPos.x, m_arena->Bounds().MinX + m_ball->HalfExtents().x };

	ballPos.x = ballPosXLeftBounds[outOfArenaXleft];

	const float ballPosXRightBounds[2] = { ballPos.x, m_arena->Bounds().MaxX - m_ball->HalfExtents().x };

	ballPos.x = ballPosXRightBounds[outOfArenaXright];

	const float ballPosYTopBounds[2] = { ballPos.y, m_arena->Bounds().MaxY - m_ball->HalfExtents().y };

	ballPos.y = ballPosYTopBounds[outOfArenaYtop];
}

void ArkanoidLogic::checkBonusCollision(const AABB& playerAABB)
{
	XMFLOAT4& bonusTranslateAndScale = m_powerUp->Transform();

	const unsigned int bonusMissed = 
		static_cast<unsigned int>(bonusTranslateAndScale.y < m_arena->Bounds().MinY - gk_destroyPowerUpYOffset);

	const AABB bonusAABB = AABB::computeFromCenterAndHalfExtents(XMFLOAT2{ bonusTranslateAndScale.x, bonusTranslateAndScale.y }, m_powerUp->HalfExtents());
	const unsigned int bonusTaken = static_cast<unsigned int>(bonusAABB.intersects(playerAABB));

	const bool destroyBonus = (bonusMissed | bonusTaken) == 1;

	//destroyBonus needs to be checked only if m_bonusAlive == true
	m_bonusAlive = (static_cast<unsigned int>(m_bonusAlive) & static_cast<unsigned int>(!destroyBonus)) == 1;

	const unsigned int bonusDead = static_cast<unsigned int>(!m_bonusAlive);

	//if the bonus is dead, translate it out of the arena, otherwise keep it in the current position
	bonusTranslateAndScale.x = static_cast<float>(bonusDead) * m_arena->OutOfArenaX() + static_cast<float>(1 - bonusDead) * bonusTranslateAndScale.x;
}

ArkanoidLogic::CollisionData ArkanoidLogic::ballAABBCollisionData(const AABB& aabb,
	const XMFLOAT2& currBallAABBMin, const XMFLOAT2& currBallAABBMax,
	const XMFLOAT2& lastBallAABBMin, const XMFLOAT2& lastBallAABBMax)
{
	auto collideFromTop = [](const XMFLOAT2& aabbMax, const XMFLOAT2& currBallAABBMin, const XMFLOAT2& lastBallAABBMin)
	{
		const float currBottom = currBallAABBMin.y;
		const float lastBottom = lastBallAABBMin.y;
		const unsigned int wasTop = static_cast<unsigned int>(lessEqualf(aabbMax.y, lastBottom));
		const unsigned int isNotTop = static_cast<unsigned int>(lessEqualf(currBottom, aabbMax.y));
		return wasTop & isNotTop;
	};

	auto collideFromBottom = [](const XMFLOAT2& aabbMin, const XMFLOAT2& currBallAABBMax, const XMFLOAT2& lastBallAABBMax)
	{
		const float currTop = currBallAABBMax.y;
		const float lastTop = lastBallAABBMax.y;
		const unsigned int wasBottom = static_cast<unsigned int>(lessEqualf(lastTop, aabbMin.y));
		const unsigned int isNotBottom = static_cast<unsigned int>(lessEqualf(aabbMin.y, currTop));
		return wasBottom & isNotBottom;
	};

	auto collideFromRight = [](const XMFLOAT2& aabbMax, const XMFLOAT2& currBallAABBMin, const XMFLOAT2& lastBallAABBMin)
	{
		const float currLeft = currBallAABBMin.x;
		const float lastLeft = lastBallAABBMin.x;
		const unsigned int wasRight = static_cast<unsigned int>(lessEqualf(aabbMax.x, lastLeft));
		const unsigned int isNotRight = static_cast<unsigned int>(lessEqualf(currLeft, aabbMax.x));
		return wasRight & isNotRight;
	};

	auto collideFromLeft = [](const XMFLOAT2& aabbMin, const XMFLOAT2& currBallAABBMax, const XMFLOAT2& lastBallAABBMax)
	{
		const float currRight = currBallAABBMax.x;
		const float lastRight = lastBallAABBMax.x;
		const unsigned int wasLeft = static_cast<unsigned int>(lessEqualf(lastRight, aabbMin.x));
		const unsigned int isNotLeft = static_cast<unsigned int>(lessEqualf(aabbMin.x, currRight));
		return wasLeft & isNotLeft;
	};

	const XMFLOAT2 aabbMin = aabb.min();
	const XMFLOAT2 aabbMax = aabb.max();

	CollisionData collisionData;
	collisionData.fromRight = collideFromRight(aabbMax, currBallAABBMin, lastBallAABBMin);
	collisionData.fromLeft = collideFromLeft(aabbMin, currBallAABBMax, lastBallAABBMax);
	collisionData.fromTop = collideFromTop(aabbMax, currBallAABBMin, lastBallAABBMin);
	collisionData.fromBottom = collideFromBottom(aabbMin, currBallAABBMax, lastBallAABBMax);

	return collisionData;
}

void ArkanoidLogic::checkBricksCollision(const AABB& currBallAABB,
	const XMFLOAT2& currBallAABBMin, const XMFLOAT2& currBallAABBMax,
	const XMFLOAT2& lastBallAABBMin, const XMFLOAT2& lastBallAABBMax)
{

	const unsigned int ballCollidersCount = m_quadtree.findPotentialColliders(currBallAABB, m_ballColliders);

	for (unsigned int colliderIndex = 0; colliderIndex < ballCollidersCount; ++colliderIndex)
	{
		const unsigned int brickIndex = m_ballColliders[colliderIndex];
		assert(brickIndex < gk_bricksCount);

		XMFLOAT4& brickTranslateAndScale = m_bricks[brickIndex]->Transform();
		const XMFLOAT2 brickAABBCenter{ brickTranslateAndScale.x, brickTranslateAndScale.y };
		const AABB aabb = AABB::computeFromCenterAndHalfExtents(brickAABBCenter, m_bricks[brickIndex]->HalfExtents());

		if (currBallAABB.intersects(aabb))
		{
			CollisionData collisionData =
				ballAABBCollisionData(aabb, currBallAABBMin, currBallAABBMax, lastBallAABBMin, lastBallAABBMax);

			//change ball velocity (simple reflection)

			const unsigned int reverseXVelocity = collisionData.fromRight | collisionData.fromLeft;
			const unsigned int reverseYVelocity = collisionData.fromTop | collisionData.fromBottom;

			const float velocitiesX[2] = { m_ball->CurrentVelocity()->x, -m_ball->CurrentVelocity()->x };
			const float velocitiesY[2] = { m_ball->CurrentVelocity()->y, -m_ball->CurrentVelocity()->y };

			m_ball->CurrentVelocity()->x = velocitiesX[reverseXVelocity];
			m_ball->CurrentVelocity()->y = velocitiesY[reverseYVelocity];

			//adjust ball position making it outside the brick

			XMFLOAT4& ballTranslationAndScale = m_ball->Transform();

			const float ballPosXRight[2] = { ballTranslationAndScale.x, brickAABBCenter.x + m_bricks[brickIndex]->HalfExtents().x + m_ball->HalfExtents().x };

			ballTranslationAndScale.x = ballPosXRight[collisionData.fromRight];

			const float ballPosXLeft[2] = { ballTranslationAndScale.x, brickAABBCenter.x - m_bricks[brickIndex]->HalfExtents().x - m_ball->HalfExtents().x };

			ballTranslationAndScale.x = ballPosXLeft[collisionData.fromLeft];

			const float ballPosYTop[2] = { ballTranslationAndScale.y, brickAABBCenter.y + m_bricks[brickIndex]->HalfExtents().y + m_ball->HalfExtents().y };

			ballTranslationAndScale.y = ballPosYTop[collisionData.fromTop];

			const float ballPosYBottom[2] = { ballTranslationAndScale.y, brickAABBCenter.y - m_bricks[brickIndex]->HalfExtents().y - m_ball->HalfExtents().y };

			ballTranslationAndScale.y = ballPosYBottom[collisionData.fromBottom];


			if (m_bricks[brickIndex]->DecreaseHitsRemaining())
			{
				XMFLOAT4& color = m_bricks[brickIndex]->Color();
				auto brickTypeIndex = getColorIndex(color);
				color = gk_bricksColors[brickTypeIndex + 1];
				m_renderer->onColorsOrUVTransformsChanged();
			}
			else
			{
				translateOutOfArena(brickTranslateAndScale);
				handleSpawnBonus(brickAABBCenter);
			}

			break;
		}
	}
}

void ArkanoidLogic::handleSpawnBonus(const XMFLOAT2& spawnPosition)
{
	++m_bonusBricksHit;

	if (m_bonusBricksHit == m_nextBonusBricksHitCount)
	{
		m_bonusBricksHit = 0;
		m_nextBonusBricksHitCount = generateNextBonusBricksHitCount();

		const unsigned int wasBonusAlive = static_cast<unsigned int>(m_bonusAlive);

		m_bonusAlive = true;

		//if wasBonusAlive the bonus position needs not to be changed to spawnPosition
		XMFLOAT4& powerUpTransformAndScale = m_powerUp->Transform();
		powerUpTransformAndScale.x = static_cast<float>(1 - wasBonusAlive) * spawnPosition.x + static_cast<float>(wasBonusAlive) * powerUpTransformAndScale.x;
		powerUpTransformAndScale.y = static_cast<float>(1 - wasBonusAlive) * spawnPosition.y + static_cast<float>(wasBonusAlive) * powerUpTransformAndScale.y;
	}
}


void ArkanoidLogic::fillUVTransforms(unsigned int atlasWidth, unsigned int atlasHeight)
{
	std::ifstream atlasDescriptorFile{ gk_texturesPath + "atlas.txt" };

	if (!atlasDescriptorFile.is_open())
	{
		assert(false);
		return;
	}

	using EntityIndicesPair = std::pair<std::string, unsigned int>;
	std::unordered_map<std::string, unsigned int> entityIndices
	{
		EntityIndicesPair{ "brick", m_bricks[0]->UvIndex()},
		EntityIndicesPair{ "ball", m_ball->UvIndex() },
		EntityIndicesPair{ "paddle", m_paddle->UvIndex() },
		EntityIndicesPair{ "arena", m_arena->UvIndex()},
		EntityIndicesPair{ "bonus", m_powerUp->UvIndex() },
		EntityIndicesPair{ "logo", m_logo->UvIndex()}
	};

	for (unsigned int entity = 0; entity < gk_entitiesCount; ++entity)
	{
		std::string textureName;
		atlasDescriptorFile >> textureName;

		char ignore;
		atlasDescriptorFile >> ignore;

		float x;
		float y;
		float width;
		float height;

		atlasDescriptorFile >> x >> y >> width >> height;

		TextureTileInfo textureTileInfo{ static_cast<float>(x),
										 static_cast<float>(y),
										 static_cast<float>(width),
										 static_cast<float>(height) };

		textureTileInfo.transformToUVSpace(atlasWidth, atlasHeight);

		const auto entityIndicesIt = entityIndices.find(textureName);
		assert(entityIndicesIt != entityIndices.end());

		const unsigned int entityIndex = entityIndicesIt->second;

		XMFLOAT4& uvTransform = m_colorsAndUVTransforms.uvTranslationAndScales[entityIndex];
		uvTransform = XMFLOAT4{ textureTileInfo.x(),
								textureTileInfo.y(),
								textureTileInfo.width(),
								textureTileInfo.height() };
	}
}

void ArkanoidLogic::assignBrickType(unsigned int brickIndex, unsigned int brickTypeIndex)
{
	m_bricks[brickIndex]->SetColor(gk_bricksColors[brickTypeIndex]);
	m_bricks[brickIndex]->SetHitsRemaining(gk_bricksHitsCounts[brickTypeIndex]);
}

unsigned int ArkanoidLogic::placeBricksRowByRow(AABB& bricksAABB)
{
	unsigned columnsCount = m_arena->Width() / gk_bricksWidth;
	unsigned rowsCount = gk_bricksCount / columnsCount;

	XMFLOAT4 currPos{ gk_bricksHalfWidth, m_arena->Bounds().MaxY - gk_bricksHeight * 2.0f, gk_bricksHalfWidth, gk_bricksHalfHeight };

	unsigned int brickIndex = 0;

	for (unsigned int row = 0; row < rowsCount; ++row)
	{
		const unsigned int brickTypeIndex = row % gk_brickTypesCount;

		for (unsigned int column = 0; column < columnsCount / 2; ++column)
		{
			assert(brickIndex < gk_bricksCount);
			unsigned int index = brickIndex++;

			m_bricks[index]->Transform() = currPos;
			assignBrickType(index, brickTypeIndex);

			assert(brickIndex < gk_bricksCount);
			index = brickIndex++;

			m_bricks[index]->Transform() = currPos;
			//reflect about y axis
			m_bricks[index]->Transform().x = -currPos.x;
			assignBrickType(index, brickTypeIndex);

			currPos.x += gk_bricksWidth;
		}
		currPos.x = gk_bricksHalfWidth;
		currPos.y -= gk_bricksHeight;
	}

	const XMFLOAT2 aabbMin{ static_cast<float>(m_arena->Bounds().MinX), currPos.y };
	const XMFLOAT2 aabbMax{ static_cast<float>(m_arena->Bounds().MaxX), m_arena->Bounds().MaxY - gk_bricksHeight };

	bricksAABB = AABB::computeFromMinMax(aabbMin, aabbMax);

	return brickIndex;
}

unsigned int ArkanoidLogic::placeBricksDiamond(AABB& bricksAABB)
{
	unsigned int brickIndex = 0;

	auto placeHalfDiamond = [this, &brickIndex](XMFLOAT4& currPos,
		unsigned int rowsCount,
		unsigned int columnsCount,
		float rowIncrementMultiplier)
	{
		unsigned int currColumnsCount = columnsCount;
		for (unsigned int row = 0; currColumnsCount > 0 && row < rowsCount; ++row)
		{
			const unsigned int brickTypeIndex = row % gk_brickTypesCount;

			for (unsigned int column = 0; column < currColumnsCount / 2; ++column)
			{
				assert(brickIndex < gk_bricksCount);
				unsigned int index = brickIndex++;

				m_bricks[index]->Transform() = currPos;
				assignBrickType(index, brickTypeIndex);

				assert(brickIndex < gk_bricksCount);
				index = brickIndex++;

				m_bricks[index]->Transform() = currPos;
				//reflect about y axis
				m_bricks[index]->Transform().x = -currPos.x;
				assignBrickType(index, brickTypeIndex);

				currPos.x += gk_bricksWidth;
			}
			currPos.x = gk_bricksHalfWidth;
			currPos.y += rowIncrementMultiplier * gk_bricksHeight;
			currColumnsCount -= 2;
		}
	};

	unsigned int columnsCount = m_arena->Width() / gk_bricksWidth;
	unsigned int rowsCount = gk_bricksCount / columnsCount; //like a box
	unsigned int halfRowsCount = rowsCount / 2;

	const float startX = gk_bricksHalfWidth;
	const float startY = m_arena->Bounds().MaxY - gk_bricksHeight * (halfRowsCount + 2);

	XMFLOAT4 currPos{ startX, startY, gk_bricksHalfWidth, gk_bricksHalfHeight };

	placeHalfDiamond(currPos, halfRowsCount, columnsCount, 1.0f);

	const float maxY = currPos.y;

	currPos.x = startX;
	currPos.y = startY - gk_bricksHeight;

	placeHalfDiamond(currPos, halfRowsCount, columnsCount, -1.0f);

	const XMFLOAT2 aabbMin{ static_cast<float>(m_arena->Bounds().MinX), currPos.y };
	const XMFLOAT2 aabbMax{ static_cast<float>(m_arena->Bounds().MaxX), maxY };

	bricksAABB = AABB::computeFromMinMax(aabbMin, aabbMax);

	return brickIndex;
}

unsigned int ArkanoidLogic::placeBricksColumnsByColumns(AABB& bricksAABB)
{
	unsigned int availableColumnsCount = m_arena->Width() / gk_bricksWidth;
	unsigned int columnsCount = 6;
	unsigned int rowsCount = gk_bricksCount / columnsCount;

	float columnsSpacing = gk_bricksWidth * (static_cast<float>(availableColumnsCount - columnsCount) / (columnsCount - 1) + 1.0f);

	float startX = m_arena->Bounds().MinX + gk_bricksHalfWidth;

	XMFLOAT4 currPos{ startX, m_arena->Bounds().MaxY - gk_bricksHeight * 2.0f, gk_bricksHalfWidth, gk_bricksHalfHeight };

	unsigned int brickIndex = 0;

	for (unsigned int row = 0; row < rowsCount; ++row)
	{
		const unsigned int brickTypeIndex = row % gk_brickTypesCount;

		for (unsigned int column = 0; column < columnsCount; ++column)
		{
			assert(brickIndex < gk_bricksCount);
			unsigned int index = brickIndex++;

			m_bricks[index]->Transform() = currPos;
			assignBrickType(index, brickTypeIndex);

			currPos.x += columnsSpacing;
		}
		currPos.x = startX;
		currPos.y -= gk_bricksHeight;
	}

	const XMFLOAT2 aabbMin{ static_cast<float>(m_arena->Bounds().MinX), currPos.y };
	const XMFLOAT2 aabbMax{ static_cast<float>(m_arena->Bounds().MaxX), static_cast<float>(m_arena->Bounds().MaxY) - gk_bricksHeight };

	bricksAABB = AABB::computeFromMinMax(aabbMin, aabbMax);

	return brickIndex;
}

void ArkanoidLogic::onBrickShuffleKeyUp()
{
	restartLevel();
}

void ArkanoidLogic::translateOutOfArena(XMFLOAT4& transform)
{
	transform.x = m_arena->OutOfArenaX();
}
