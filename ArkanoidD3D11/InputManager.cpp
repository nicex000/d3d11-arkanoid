#include "InputManager.h"
#include "Window.h"
#include "ArkanoidLogic.h"

using namespace ArkanoidGame;
using namespace ArkanoidEngine;

InputManager::InputManager(Window& window, ArkanoidLogic& arkanoid) : m_window{ window }, m_arkanoid{ arkanoid }
{
	m_window.addWindowMessagesHandler(this);
}

InputManager::~InputManager()
{
	m_window.removeWindowMessagesHandler(this);
}

LRESULT InputManager::handleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_KEYDOWN:
		if (onKeyDown(wParam))
		{
			return 0;
		}
		break;
	case WM_KEYUP:
		if (onKeyUp(wParam))
		{
			return 0;
		}
		break;
	}

	return 1;
}

static constexpr WPARAM gk_leftKeyCode = 0x41; //A
static constexpr WPARAM gk_leftArrowKeyCode = VK_LEFT; //LArrow
static constexpr WPARAM gk_rightKeyCode = 0x44; //D
static constexpr WPARAM gk_rightArrowKeyCode = VK_RIGHT; //RArrow
static constexpr WPARAM gk_brickShuffleKeyCode = VK_SPACE;

bool InputManager::onKeyDown(WPARAM keyCode)
{
	switch (keyCode)
	{
	case gk_leftKeyCode: case gk_leftArrowKeyCode:
		onLeftKeyDown();
		return true;
	case gk_rightKeyCode: case gk_rightArrowKeyCode:
		onRightKeyDown();
		return true;
	case gk_brickShuffleKeyCode:
		onBrickShuffleKeyDown();
		return true;
	}

	return false;
}

bool InputManager::onKeyUp(WPARAM keyCode)
{
	switch (keyCode)
	{
	case gk_leftKeyCode: case gk_leftArrowKeyCode:
		onLeftKeyUp();
		return true;
	case gk_rightKeyCode: case gk_rightArrowKeyCode:
		onRightKeyUp();
		return true;
	case gk_brickShuffleKeyCode:
		onBrickShuffleKeyUp();
		return true;
	}

	return false;
}

void InputManager::onLeftKeyDown()
{
	m_leftPressed = true;
	m_arkanoid.onLeftKeyDown();
}

void InputManager::onLeftKeyUp()
{
	m_leftPressed = false;
	m_arkanoid.onLeftKeyUp();
}

void InputManager::onRightKeyDown()
{
	m_rightPressed = true;
	m_arkanoid.onRightKeyDown();
}

void InputManager::onRightKeyUp()
{
	m_rightPressed = false;
	m_arkanoid.onRightKeyUp();
}

void InputManager::onBrickShuffleKeyDown()
{
	m_arkanoid.onBrickShuffleKeyDown();
}

void InputManager::onBrickShuffleKeyUp()
{
	m_arkanoid.onBrickShuffleKeyUp();
}
