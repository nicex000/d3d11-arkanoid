#pragma once
#include "Object.h"
class PowerUp :
    public Object
{
private:
    float fallSpeed;

public:
    PowerUp(unsigned int transformIndex, unsigned int UVTransformIndex, float halfWidth, float halfHeight, float Speed);
    float FallSpeed() const;
};

