#pragma once
#include "Object.h"
class Paddle :
    public Object
{
private:
    float speed;

public:
    Paddle(unsigned int transformIndex, unsigned int UVTransformIndex, float halfWidth, float halfHeight, float Speed);
    float Speed() const;
};

