#include "Arena.h"

Arena::Arena(unsigned transformIndex, unsigned UVTransformIndex, float halfWidth, float halfHeight):
	Object(transformIndex, UVTransformIndex, halfWidth, halfHeight), bounds(halfWidth, halfHeight)
{
}

XMFLOAT2 Arena::Extents() const
{
	return XMFLOAT2{ halfExtents.x *2.f, halfExtents.y * 2.f};
}

float Arena::Width() const
{
	return halfExtents.x * 2.f;
}

float Arena::Height() const
{
	return halfExtents.y * 2.f;
}

const struct Arena::Bounds& Arena::Bounds() const
{
	return bounds;
}

float Arena::OutOfArenaX() const
{
	return bounds.MaxX * 10.f;
}
