#include "Ball.h"

Ball::Ball(unsigned transformIndex, unsigned UVTransformIndex, float halfWidth, float halfHeight, float Speed)
	: Object(transformIndex, UVTransformIndex, halfWidth, halfHeight), speed(Speed)
{
	currentVelocity = new XMFLOAT2{};
}

Ball::~Ball()
{
	delete currentVelocity;
}

float Ball::Speed() const
{
	return speed;
}

XMFLOAT2* Ball::CurrentVelocity() const
{
	return currentVelocity;
}
