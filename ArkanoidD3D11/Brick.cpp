#include "Brick.h"

Brick::Brick(unsigned transformIndex, unsigned UVTransformIndex, float halfWidth, float halfHeight) :
	Object(transformIndex, UVTransformIndex, halfWidth, halfHeight)
{
}

int Brick::HitsRemaining() const
{
	return hitsRemaining;
}

void Brick::SetHitsRemaining(int hits_remaining)
{
	hitsRemaining = hits_remaining;
}

bool Brick::DecreaseHitsRemaining()
{
	--hitsRemaining;
	return hitsRemaining > 0;
}
