#pragma once
#include "Object.h"
class Arena :
    public Object
{
public:
    struct Bounds
    {
        float MinX;
        float MinY;
        float MaxX;
        float MaxY;
        Bounds(float halfWidth, float halfHeight)
        {
            MinX = -halfWidth;
            MinY = -halfHeight;
            MaxX = halfWidth;
            MaxY = halfHeight;
        }

        explicit Bounds(XMFLOAT2 halfExtents) : Bounds(halfExtents.x, halfExtents.y) {}

    };

private:
    Bounds bounds;
public:
    
    Arena(unsigned int transformIndex, unsigned int UVTransformIndex, float halfWidth, float halfHeight);
    
    XMFLOAT2 Extents() const;
    float Width() const;
    float Height() const;
    const Bounds& Bounds() const;
    float OutOfArenaX() const;
};

